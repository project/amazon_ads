INTRODUCTION
------------

The Amazon Ads module enable an easy way to integrate
Amazon Native Shopping Ads to any section of the website.

 * For a full description of the module:
   https://drupal.org/project/amazon_ads

 * To submit bug reports and feature suggestions:
   https://drupal.org/project/issues/amazon_ads


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions
  (admin/people/permissions)

   - Administer Amazon Ads module

     Users in roles with the "Administer Amazon Ads module" permission
     will have access to the configuration page of the module.

 * Configure the Amazon Ads settings in Administration » Configuration »
   Web Services » Amazon Ads. (admin/config/services/amazonads)


USAGE
-----

 * The module provide a block "Amazon Ads" available in the list of blocks.
   Could be used in any section of the website.

 * For developers, could call the function amazon_ads_widget() in your code.
   The function displays a section with the Amazon Ads embed.


TROUBLESHOOTING
---------------

 * If the entry in the menu Services does not display, check the following:

   - Are the "Administer Amazon Ads module" permission enabled
   for the appropriate roles?


MAINTAINERS
-----------

Author:
 * Rebeca Escandon (rescandon) https://www.drupal.org/u/rescandon
